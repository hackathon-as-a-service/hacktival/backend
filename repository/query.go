package repository

// Edge wraps a lead resource and procides a cursor used for pagination.
type Edge struct {
	Cursor string
	Node   *Repo
}

// Window represents a window into a list of lead resources.
type Window struct {
	ID           string
	EndCursor    string
	Edges        []*Edge
	ContainsMore bool
}

// NewWindow creates a new lead window,
func NewWindow(id string, edges []*Edge, containsMore bool) *Window {
	endCursor := ""
	if len(edges) > 0 {
		endCursor = edges[len(edges)-1].Cursor
	}

	return &Window{
		ID:           id,
		EndCursor:    endCursor,
		Edges:        edges,
		ContainsMore: containsMore,
	}
}
