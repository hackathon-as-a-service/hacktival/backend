package repository

// Service contails all repo related logic
type Service interface {
	// GetByLeadID returns all by the given lead owned repositories
	GetByLeadID(id string) ([]*Repo, error)

	// UpsertMany inserts the given entries into the database or updates them if they
	// already exist
	UpsertMany(params []*Params) error
}

// service is a basic implementation of Service
type service struct {
	repo Repository
}

// NewService creates a new Service
func NewService(repo Repository) Service {
	return &service{
		repo: repo,
	}
}

// GetByLeadID returns all by the given lead owned repositories
func (s *service) GetByLeadID(id string) ([]*Repo, error) {
	return s.repo.GetByLeadID(id)
}

// UpsertMany inserts the given entries into the database or updates them if they
// already exist
func (s *service) UpsertMany(params []*Params) error {
	repos := make([]*Repo, len(params))
	for i, p := range params {
		r, err := New(p)
		if err != nil {
			return err
		}

		repos[i] = r
	}

	return s.repo.UpsertMany(repos)
}
