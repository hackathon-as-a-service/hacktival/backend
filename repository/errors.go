package repository

import "errors"

var (
	// ErrUnknownScrapingStatus occurs when a string is parsed which isn't contained in the known scraping status.
	ErrUnknownScrapingStatus = errors.New("unknown scraping status")
)
