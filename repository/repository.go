package repository

// Repository is used to perform CRUD operations on lead resources.
type Repository interface {
	// GetByLeadID returns all by the given lead owned repositories
	GetByLeadID(id string) ([]*Repo, error)

	// UpsertMany inserts the given entries into the database or updates them if they
	// already exist
	UpsertMany(repos []*Repo) error
}
