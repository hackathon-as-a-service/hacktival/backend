package repository

import "time"

// Repo represents a single github repository.
type Repo struct {
	ID             string    `json:"id,omitempty"`
	URL            string    `json:"url,omitempty"`
	UpdatedAt      time.Time `json:"updated_at,omitempty"`
	CreatedAt      time.Time `json:"created_at,omitempty"`
	Description    string    `json:"description,omitempty"`
	HomepageURL    string    `json:"homepage_url,omitempty"`
	ForkCount      int       `json:"fork_count,omitempty"`
	IsFork         bool      `json:"is_fork,omitempty"`
	Name           string    `json:"name,omitempty"`
	StarGazerCount int       `json:"star_gazer_count,omitempty"`
	WatcherCount   int       `json:"watcher_count,omitempty"`
	Languages      []string  `json:"languages,omitempty"`
}

// Params are the params allowed during repository creation / updates.
type Params struct {
	ID             string    `json:"id,omitempty"`
	URL            string    `json:"url,omitempty"`
	UpdatedAt      time.Time `json:"updated_at,omitempty"`
	CreatedAt      time.Time `json:"created_at,omitempty"`
	Description    string    `json:"description,omitempty"`
	HomepageURL    string    `json:"homepage_url,omitempty"`
	ForkCount      int       `json:"fork_count,omitempty"`
	IsFork         bool      `json:"is_fork,omitempty"`
	Name           string    `json:"name,omitempty"`
	StarGazerCount int       `json:"star_gazer_count,omitempty"`
	WatcherCount   int       `json:"watcher_count,omitempty"`
	Languages      []string  `json:"languages,omitempty"`
}

// New creates a new repository.
func New(p *Params) (*Repo, error) {
	if err := p.Validate(); err != nil {
		return nil, err
	}

	return &Repo{
		ID:             p.ID,
		URL:            p.URL,
		UpdatedAt:      p.UpdatedAt,
		CreatedAt:      p.CreatedAt,
		Description:    p.Description,
		HomepageURL:    p.HomepageURL,
		ForkCount:      p.ForkCount,
		IsFork:         p.IsFork,
		Name:           p.Name,
		StarGazerCount: p.StarGazerCount,
		WatcherCount:   p.WatcherCount,
		Languages:      p.Languages,
	}, nil
}

// Validate validates the given params and return nil if the params are valid.
func (p *Params) Validate() error {
	return nil
}
