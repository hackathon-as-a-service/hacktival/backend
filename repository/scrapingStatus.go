package repository

import "strings"

// ScrapingStatus represents the scraping status of a single lead.
type ScrapingStatus string

const (
	// Scraped is the scraping status of a successfully scraped lead.
	Scraped ScrapingStatus = "scraped"

	// InProgress is the scraping status of a lead fow whom a scraping job was created but isn't finished yet.
	InProgress ScrapingStatus = "in_progress"

	// NotProcessed is the scraping status of a non processed lead.
	NotProcessed ScrapingStatus = "not_processed"
)

// ScrapingStatusFromString returns the to the given string belonging scraping status.
func ScrapingStatusFromString(s string) (ScrapingStatus, error) {
	switch strings.ToLower(s) {
	case string(Scraped):
		return Scraped, nil
	case string(InProgress):
		return InProgress, nil
	case string(NotProcessed):
		return NotProcessed, nil
	default:
		return "", ErrUnknownScrapingStatus
	}
}

// String returns the string representation of the scraping status.
func (s ScrapingStatus) String() string {
	return string(s)
}
