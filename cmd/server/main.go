package main

import (
	"fmt"

	"github.com/couchbase/gocb"
	"github.com/go-chi/chi"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/config"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/contribution"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/couchbase"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/graphql/graph"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/http"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/lead"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/repository"
	"go.uber.org/dig"
)

func main() {
	container := dig.New()

	container.Provide(config.NewService)
	container.Provide(func(c config.Service) config.Config {
		return c.Config()
	})

	container.Provide(func(c config.Config) (*gocb.Bucket, error) {
		cluster, err := gocb.Connect(c.Couchbase.URL)
		if err != nil {
			return nil, err
		}

		if err := cluster.Authenticate(gocb.PasswordAuthenticator{
			Username: c.Couchbase.User,
			Password: c.Couchbase.Password,
		}); err != nil {
			return nil, err
		}

		return cluster.OpenBucket(c.Couchbase.Bucket, "")
	})

	container.Provide(couchbase.NewLeadRepository)
	container.Provide(couchbase.NewRepoRepository)
	container.Provide(couchbase.NewContributionRespsitory)

	container.Provide(lead.NewService)
	container.Provide(contribution.NewService)
	container.Provide(repository.NewService)

	container.Provide(graph.NewResolverRoot)
	container.Provide(graph.NewLeadResolver)
	container.Provide(graph.NewLeadWindowResolver)
	container.Provide(graph.NewQueryResolver)
	container.Provide(graph.NewMutationResolver)
	container.Provide(http.NewGraphQL)

	if err := container.Invoke(func(c config.Config, gqlHandler *http.GraphQLHandler) {
		r := chi.NewRouter()
		r.Use(http.CORSWhiteList(c.CORSAllowedOrigins))

		gqlHandler.RegisterRoutes(r)

		s := http.NewServer(fmt.Sprintf(":%d", c.Port), r)
		fmt.Printf("Starting server on port %d", c.Port)
		if err := s.ListenAndServe(); err != nil {
			panic(err)
		}
	}); err != nil {
		panic(err)
	}
}
