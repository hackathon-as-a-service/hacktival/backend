package uuid

import (
	"strings"

	uuid "github.com/satori/go.uuid"
)

// New creates a new, unique UUIDv4
func New() string {
	return uuid.NewV4().String()
}

// NewFromParts serializes a id from the given parts
func NewFromParts(parts ...string) string {
	return strings.Join(parts, ":")
}
