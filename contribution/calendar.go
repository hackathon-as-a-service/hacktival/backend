package contribution

import (
	"math"
	"strconv"
	"time"

	"gitlab.com/hackathon-as-a-service/hacktival/backend/uuid"
)

// CalendarWindow provides a window into the contribution calendar.
type CalendarWindow struct {
	From    time.Time
	To      time.Time
	Entries []*CalendarEntry
}

// CalendarEntryParams represents params allowed during CalendarEntry creation / updates
type CalendarEntryParams struct {
	LeadID            string    `json:"lead_id,omitempty"`
	Date              time.Time `json:"date,omitempty"`
	ContributionCount int       `json:"contribution_count,omitempty"`
}

// CalendarEntry represents a single entry in the contribution calendar
type CalendarEntry struct {
	ID                string    `json:"id,omitempty"`
	LeadID            string    `json:"lead_id,omitempty"`
	Date              time.Time `json:"date,omitempty"`
	ContributionCount int       `json:"contribution_count,omitempty"`
}

// NewCalendarEntry creates a new calendar entry.
func NewCalendarEntry(p *CalendarEntryParams) *CalendarEntry {
	return &CalendarEntry{
		ID:                uuid.NewFromParts(p.LeadID, strconv.FormatFloat(math.Floor(float64(p.Date.Unix())/60/60/24), 'f', 0, 64)),
		LeadID:            p.LeadID,
		Date:              p.Date,
		ContributionCount: p.ContributionCount,
	}
}
