package contribution

import "time"

// Query is used to query contribution callendar resources.
type Query struct {
	LeadID string
	From   time.Time
	To     time.Time
}

// Window represents a window into a list of lead resources.
type Window struct {
	ID    string
	From  time.Time
	To    time.Time
	Edges []*CalendarEntry
}

// NewWindow crates a new contribution calendar window
func NewWindow(id string, from, to time.Time, edges []*CalendarEntry) *Window {
	return &Window{
		ID:    id,
		From:  from,
		To:    to,
		Edges: edges,
	}
}
