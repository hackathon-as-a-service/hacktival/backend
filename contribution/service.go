package contribution

// Service contains all contribution related logic.
type Service interface {
	// Find is used to query contribution calendar entries
	Find(query *Query) (*Window, error)

	// UpsertMany inserts the given entries into the database or updates them if they
	// already exist
	UpsertMany(params []*CalendarEntryParams) error
}

// service is a basic implementation of service.
type service struct {
	repo Repository
}

// NewService crates a new basic Service
func NewService(repo Repository) Service {
	return &service{
		repo: repo,
	}
}

// Find is used to query contribution calendar entries
func (s *service) Find(q *Query) (*Window, error) {
	return s.repo.Find(q)
}

// UpsertMany inserts the given entries into the database or updates them if they
// already exist
func (s *service) UpsertMany(params []*CalendarEntryParams) error {
	entries := make([]*CalendarEntry, len(params))
	for i, p := range params {
		entries[i] = NewCalendarEntry(p)
	}

	return s.repo.UpsertMany(entries)
}
