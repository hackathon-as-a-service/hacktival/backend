package contribution

// Repository is used to perform CRUD operations on lead resources.
type Repository interface {
	// Find is used to query calendar entries.
	Find(query *Query) (*Window, error)

	// UpsertMany inserts the given entries into the database or updates them if they
	// already exist
	UpsertMany(entries []*CalendarEntry) error
}
