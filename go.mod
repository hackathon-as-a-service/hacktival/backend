module gitlab.com/hackathon-as-a-service/hacktival/backend

go 1.12

require (
	cloud.google.com/go v0.38.0 // indirect
	github.com/99designs/gqlgen v0.8.3
	github.com/couchbase/gocb v1.6.1
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/websocket v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/satori/go.uuid v1.2.0
	github.com/vektah/gqlparser v1.1.2
	go.uber.org/dig v1.7.0
	golang.org/x/exp/errors v0.0.0-20190510132918-efd6b22b2522 // indirect
	golang.org/x/net v0.0.0-20190509222800-a4d6f7feada5 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/couchbase/gocbcore.v7 v7.1.13 // indirect
	gopkg.in/couchbaselabs/gocbconnstr.v1 v1.0.2 // indirect
	gopkg.in/couchbaselabs/gojcbmock.v1 v1.0.3 // indirect
	gopkg.in/couchbaselabs/jsonx.v1 v1.0.0 // indirect
)
