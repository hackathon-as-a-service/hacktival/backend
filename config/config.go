package config

// Config represents the application configuration.
type Config struct {
	Port               int      `default:"8001"`
	LoggingLevel       string   `default:"debug" envconfig:"logging_level"`
	CORSAllowedOrigins []string `envconfig:"cors_allowed_origins"`

	Couchbase struct {
		URL      string `required:"true" envconfig:"couchbase_url"`
		User     string `required:"true" envconfig:"couchbase_username"`
		Password string `required:"true" envconfig:"couchbase_password"`
		Bucket   string `required:"true" envconfig:"couchbase_bucket"`
	}
}
