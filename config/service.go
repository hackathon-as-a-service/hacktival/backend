package config

import (
	"github.com/kelseyhightower/envconfig"
)

// Service is used to load the application config
type Service interface {
	// Config returns the application config
	Config() Config
}

// service is a basic environment based implementation of service
type service struct {
	config Config
}

// NewService creates a new config service
func NewService() (Service, error) {
	config := Config{}
	if err := envconfig.Process("", &config); err != nil {
		return nil, err
	}

	return &service{config}, nil
}

// Config returns the application config
func (s *service) Config() Config {
	return s.config
}
