package lead

import (
	"time"
)

// Lead represents a scraped social media user
type Lead struct {
	Score          float64        `json:"score,omitempty"`
	LanguageScores LanguageScores `json:"language_scores,omitempty"`
	WasContacted   bool           `json:"was_contacted,omitempty"`
	ScrapingStatus ScrapingStatus `json:"scraping_status,omitempty"`
	ID             string         `json:"id,omitempty"`
	AvatarURL      string         `json:"avatar_url,omitempty"`
	Bio            string         `json:"bio,omitempty"`
	Company        string         `json:"company,omitempty"`
	CreatedAt      time.Time      `json:"created_at,omitempty"`
	WebsiteURL     string         `json:"website_url,omitempty"`
	Email          string         `json:"email,omitempty"`
	Location       string         `json:"location,omitempty"`
	Login          string         `json:"login,omitempty"`
	Creation       time.Time      `json:"creation,omitempty"`
}

// Params contains all params allowed during lead creation / updatedating
type Params struct {
	WasContacted bool
	ID           string
	AvatarURL    string
	Bio          string
	Company      string
	Languages    []string
	CreatedAt    time.Time
	WebsiteURL   string
	Email        string
	Location     string
	Login        string
}

// New creates a new lead based on the given params
func New(p *Params) (*Lead, error) {
	if err := p.Validate(); err != nil {
		return nil, err
	}

	return &Lead{
		ID:           p.ID,
		AvatarURL:    p.AvatarURL,
		Bio:          p.Bio,
		Company:      p.Company,
		CreatedAt:    p.CreatedAt,
		WebsiteURL:   p.WebsiteURL,
		Email:        p.Email,
		Location:     p.Location,
		WasContacted: p.WasContacted,
		Login:        p.Login,
		Creation:     time.Now().UTC(),
	}, nil
}

// Update updates the given lead according to the given params.
func (l *Lead) Update(p *Params) {
	l.AvatarURL = p.AvatarURL
	l.Bio = p.Bio
	l.Company = p.Company
	l.CreatedAt = p.CreatedAt
	l.WebsiteURL = p.WebsiteURL
	l.Email = p.Email
	l.Location = p.Location
	l.Login = p.Login
}

// Validate validates the given params and return nil if the params are valid.
func (p *Params) Validate() error {
	return nil
}
