package lead

// Repository is used to perform CRUD operations on lead resources.
type Repository interface {
	// Find is used to query leads.
	Find(query *Query) (*Window, error)

	// FindCount returns the count of results for the 
	FindCount(query *Query) (int, error)

	// Get returns the lead with the given id.
	Get(id string) (*Lead, error)

	// GetMany returns all leads with the given ids.
	GetMany(ids []string) ([]*Lead, []error)

	// Insert inserts the given lead into the database.
	Insert(lead *Lead) error

	// Update updates the given lead, the lead will be identified by his id.
	Update(lead *Lead) error

	// UpsertMany inserts the given entries into the database or updates them if they
	// already exist
	UpsertMany(leads []*Lead) error

	// Delete deletes the lead with the given id.
	Delete(id string) error
}
