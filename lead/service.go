package lead

import "gitlab.com/hackathon-as-a-service/hacktival/backend/repository"

// Service contains all lead related logic.
type Service interface {
	// Find is used to query leads.
	Find(q *Query) (*Window, error)

	GetTotalCount(window *Window) (int, error)

	// Get returns the Lead with the given id
	Get(id string) (*Lead, error)

	// GetNextToScrape returns the next accounts to scrape.
	GetNextToScrape(count int) ([]*Lead, error)

	// UpsertMany inserts the given entries into the database or updates them if they
	// already exist
	UpsertMany(params []*Params) error

	// Create creates a lead based on the given params.
	Create(p *Params) (*Lead, error)

	// Evaluate evaluates the lead with the given id.
	Evaluate(id string) (*Lead, error)
}

// service is a basic implementation of Service
type service struct {
	repo    Repository
	repoSrv repository.Service
}

// NewService creates a new, basic service.
func NewService(repo Repository) Service {
	return &service{
		repo: repo,
	}
}

// Find is used to query leads.s
func (s *service) Find(q *Query) (*Window, error) {
	return s.repo.Find(q)
}

func (s *service) GetTotalCount(win *Window) (int, error) {
	return s.repo.FindCount(win.query)
}

// Get returns the Lead with the given id
func (s *service) Get(id string) (*Lead, error) {
	return s.repo.Get(id)
}

// GetNextToScrape returns the next accounts to scrape
func (s *service) GetNextToScrape(count int) ([]*Lead, error) {
	scrapingStatus := NotProcessed
	window, err := s.repo.Find(&Query{
		Next:           count,
		ScrapingStatus: &scrapingStatus,
	})

	if err != nil {
		return nil, err
	}

	leads := []*Lead{}
	for _, edge := range window.Edges {
		leads = append(leads, edge.Node)
	}

	return leads, nil
}

// Evaluate evalutes the lead with the given id.
func (s *service) Evaluate(id string) (*Lead, error) {
	lead, err := s.repo.Get(id)
	if err != nil {
		return nil, err
	}

	repos, err := s.repoSrv.GetByLeadID(id)
	if err != nil {
		return nil, err
	}

	scores := LanguageScores{}
	for _, repo := range repos {
		scores.CountRepo(repo)
	}

	scores.Calculate()
	lead.Score = scores.TotalScore()
	lead.ScrapingStatus = Scraped

	if err := s.repo.Insert(lead); err != nil {
		return nil, err
	}

	return lead, nil
}

// Create creates a lead based on the given params.
func (s *service) Create(p *Params) (*Lead, error) {
	lead, err := New(p)
	if err != nil {
		return nil, err
	}

	if err := s.repo.Insert(lead); err != nil {
		return nil, err
	}

	return lead, nil
}

// UpsertMany inserts the given entries into the database or updates them if they
// already exist
func (s *service) UpsertMany(params []*Params) error {
	ids := make([]string, len(params))
	for i, p := range params {
		ids[i] = p.ID
	}

	leads, errs := s.repo.GetMany(ids)
	for i, l := range leads {
		err := errs[i]
		if err != nil && err != ErrNotFound {
			return err
		}

		if err == nil {
			l.Update(params[i])
			leads[i] = l
			continue
		}

		l, err := New(params[i])
		if err != nil {
			return err
		}

		leads[i] = l
	}

	return s.repo.UpsertMany(leads)
}
