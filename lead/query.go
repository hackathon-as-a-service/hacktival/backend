package lead

import (
	"errors"
	"strings"

	"gitlab.com/hackathon-as-a-service/hacktival/backend/uuid"
)

// OrderType is used to specify the order leads are retrived from the database.
type OrderType string

const (
	// Creation is the sort order used to order by date the lead was scraped
	Creation OrderType = "creation"

	// Score is the sort order used to order leads by their score
	Score OrderType = "score"
)

// Query is used to query lead resources.
type Query struct {
	Cursor         string
	Languages      []string
	MinScore       float64
	MaxScore       float64
	Next           int
	OrderType      OrderType
	Ascending      bool
	ScrapingStatus *ScrapingStatus
}

// Edge wraps a lead resource and procides a cursor used for pagination.
type Edge struct {
	ID     string
	Cursor string
	Node   *Lead
}

// Window represents a window into a list of lead resources.
type Window struct {
	query        *Query
	ID           string
	EndCursor    string
	Edges        []*Edge
	ContainsMore bool
}

// NewEdge creates a new edge.
func NewEdge(lead *Lead, cursor string) *Edge {
	return &Edge{
		ID:     uuid.NewFromParts(lead.ID, cursor),
		Cursor: cursor,
		Node:   lead,
	}
}

// NewWindow creates a new lead window,
func NewWindow(query *Query, id string, edges []*Edge, containsMore bool) *Window {
	endCursor := ""
	if len(edges) > 0 {
		endCursor = edges[len(edges)-1].Cursor
	}

	return &Window{
		query:        query,
		ID:           id,
		EndCursor:    endCursor,
		Edges:        edges,
		ContainsMore: containsMore,
	}
}

// String returns the string representation of the given order type.
func (o OrderType) String() string {
	return string(o)
}

// SortOrderFromString returns the to the given string belonging sort order.
func SortOrderFromString(s string) (OrderType, error) {
	switch strings.ToLower(s) {
	case string(Creation):
		return Creation, nil
	case string(Score):
		return Score, nil
	default:
		return "", errors.New("unknown order type")
	}
}
