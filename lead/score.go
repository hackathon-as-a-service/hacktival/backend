package lead

import (
	"math"
	"math/rand"

	"gitlab.com/hackathon-as-a-service/hacktival/backend/repository"
)

// LanguageScores contains the scores of a single lead for all
type LanguageScores map[string]*ScoreInfo

// ScoreInfo contains score relates information and the score itself
type ScoreInfo struct {
	Score          float64 `json:"score,omitempty"`
	WatcherCount   int     `json:"watcher_count,omitempty"`
	StarGazerCount int     `json:"star_gazer_count,omitempty"`
	ForkCount      int     `json:"fork_count,omitempty"`
}

// CountRepo counts the given repo inside the users score.
func (l LanguageScores) CountRepo(repo *repository.Repo) {
	for _, lang := range repo.Languages {
		if info, ok := l[lang]; ok {
			info.WatcherCount += repo.WatcherCount
			info.StarGazerCount += repo.StarGazerCount
			info.ForkCount += repo.ForkCount
		}
	}
}

// Calculate calculates the language scores based on the contained information
func (l LanguageScores) Calculate() {
	for _, info := range l {
		watcherScore := normaizedScore(float64(info.WatcherCount), 2, 100)
		starGazerScore := normaizedScore(float64(info.StarGazerCount), 3, 300)
		forkCountScore := normaizedScore(float64(info.ForkCount), 0, 100)

		info.Score = watcherScore*0.40 + starGazerScore*0.4 + forkCountScore*0.15 + rand.Float64()*0.05
	}
}

// TotalScore returns the users score
func (l LanguageScores) TotalScore() float64 {
	totalScore := 0.0
	for _, info := range l {
		totalScore += info.Score
	}

	return math.Max(0, math.Min(1, totalScore/(float64(len(l))*0.8)*0.8-rand.Float64()*0.05))
}

func normaizedScore(score, min, max float64) float64 {
	score = math.Min(math.Max(score-min, 0), max)
	return math.Log(score*0.5+1) / math.Log(max*0.5+1)
}
