package lead

import "errors"

var (
	// ErrUnknownScrapingStatus occurs when a string is parsed which isn't contained in the known scraping status.
	ErrUnknownScrapingStatus = errors.New("unknown scraping status")

	// ErrNotFound occurs when a operation is performed on a non existing lead.
	ErrNotFound = errors.New("not found")
)
