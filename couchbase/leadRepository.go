package couchbase

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/couchbase/gocb"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/lead"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/uuid"
)

type leadRepository struct {
	bucket *gocb.Bucket
}

// NewLeadRepository creates a new couchbase based lead.Repository
func NewLeadRepository(bucket *gocb.Bucket) lead.Repository {
	return &leadRepository{
		bucket: bucket,
	}
}

func (r *leadRepository) Find(query *lead.Query) (*lead.Window, error) {
	params := []interface{}{LeadType}
	queryString := fmt.Sprintf("SELECT bacon.* FROM `%s` bacon WHERE type = $1", r.bucket.Name())

	queryString, params = r.appendFilerConditions(query, queryString, params)

	if query.OrderType == lead.Creation {
		if query.Cursor != "" {
			params = append(params, query.Cursor)
			queryString += " AND base.creation "

			if query.Ascending {
				queryString += fmt.Sprintf(" > $%d", len(params))
			} else {
				queryString += fmt.Sprintf(" < $%d", len(params))
			}
		}

		queryString += " ORDER BY base.creation"
	} else {
		if query.Cursor != "" {
			params = append(params, query.Cursor)
			queryString += " AND base.score "

			if query.Ascending {
				queryString += fmt.Sprintf(" > $%d", len(params))
			} else {
				queryString += fmt.Sprintf(" < $%d", len(params))
			}
		}

		queryString += " ORDER BY base.score"
	}

	if query.Ascending {
		queryString += " ASC"
	} else {
		queryString += " DESC"
	}

	queryString += fmt.Sprintf(" LIMIT %d", query.Next)

	res, err := r.bucket.ExecuteN1qlQuery(gocb.NewN1qlQuery(queryString), params)
	if err != nil {
		return nil, err
	}

	edges := []*lead.Edge{}
	doc := &Document{Base: &lead.Lead{}}
	for res.Next(doc) {
		l := doc.Base.(*lead.Lead)
		doc = &Document{Base: &lead.Lead{}}

		if query.OrderType == lead.Creation {
			edges = append(edges, lead.NewEdge(l, l.Creation.String()))
		} else {
			edges = append(edges, lead.NewEdge(l, strconv.FormatFloat(l.Score, 'f', -1, 64)))
		}
	}

	return lead.NewWindow(query, r.queryToWindowID(query), edges, len(edges) == query.Next), nil
}

func (r *leadRepository) FindCount(query *lead.Query) (int, error) {
	params := []interface{}{LeadType}
	queryString := fmt.Sprintf("SELECT COUNT(*) as count FROM `%s` bacon WHERE type = $1", r.bucket.Name())
	queryString, params = r.appendFilerConditions(query, queryString, params)

	res, err := r.bucket.ExecuteN1qlQuery(gocb.NewN1qlQuery(queryString), params)
	if err != nil {
		return 0, err
	}

	count := &CountResult{}
	if err := res.One(count); err != nil {
		return 0, err
	}

	return count.Count, nil
}

func (r *leadRepository) Get(id string) (*lead.Lead, error) {
	doc := &Document{Base: &lead.Lead{}}
	_, err := r.bucket.Get(SerializeKey(LeadType, id), doc)
	return doc.Base.(*lead.Lead), r.toInternalError(err)
}

func (r *leadRepository) GetMany(ids []string) ([]*lead.Lead, []error) {
	blkOP := make([]gocb.BulkOp, len(ids))
	for i, id := range ids {
		blkOP[i] = &gocb.GetOp{
			Key:   SerializeKey(LeadType, id),
			Value: &Document{Base: &lead.Lead{}},
		}
	}

	errs := make([]error, len(ids))
	err := r.bucket.Do(blkOP)
	if err != nil {
		for i := range errs {
			errs[i] = r.toInternalError(err)
		}

		return nil, errs
	}

	leads := make([]*lead.Lead, len(ids))
	for i, op := range blkOP {
		getOp := op.(*gocb.GetOp)
		if getOp.Err != nil {
			errs[i] = r.toInternalError(getOp.Err)
			continue
		}

		leads[i] = getOp.Value.(*lead.Lead)
	}

	return leads, errs
}

func (r *leadRepository) Insert(lead *lead.Lead) error {
	_, err := r.bucket.Insert(SerializeKey(LeadType, lead.ID), NewDocument(LeadType, 1, lead), 0)
	return r.toInternalError(err)
}

func (r *leadRepository) Update(lead *lead.Lead) error {
	_, err := r.bucket.Replace(SerializeKey(LeadType, lead.ID), NewDocument(LeadType, 1, lead), 0, 0)
	return r.toInternalError(err)
}

func (r *leadRepository) UpsertMany(leads []*lead.Lead) error {
	blkOP := make([]gocb.BulkOp, len(leads))

	for i, r := range leads {
		blkOP[i] = &gocb.UpsertOp{
			Key:   SerializeKey(LeadType, r.ID),
			Value: NewDocument(LeadType, 1, r),
		}
	}

	if err := r.bucket.Do(blkOP); err != nil {
		return r.toInternalError(err)
	}

	for _, val := range blkOP {
		if err := val.(*gocb.UpsertOp).Err; err != nil {
			return r.toInternalError(err)
		}
	}

	return nil
}

func (r *leadRepository) Delete(id string) error {
	_, err := r.bucket.Remove(SerializeKey(LeadType, id), 0)
	return r.toInternalError(err)
}

func (r *leadRepository) toInternalError(err error) error {
	switch err {
	case gocb.ErrKeyNotFound:
		return lead.ErrNotFound
	default:
		return err
	}
}

func (r *leadRepository) queryToWindowID(query *lead.Query) string {
	return uuid.NewFromParts(
		strings.Join(query.Languages, ","),
		strconv.FormatFloat(query.MinScore, 'f', -1, 64),
		strconv.FormatFloat(query.MaxScore, 'f', -1, 64),
		query.OrderType.String(),
		strconv.FormatBool(query.Ascending),
		query.ScrapingStatus.String(),
	)
}

func (r *leadRepository) appendFilerConditions(query *lead.Query, queryString string, params []interface{}) (string, []interface{}) {
	if query.MinScore > 0 {
		params = append(params, query.MinScore)
		queryString += fmt.Sprintf(" AND base.score > $%d", len(params))
	}

	if query.MaxScore < 1 {
		params = append(params, query.MaxScore)
		queryString += fmt.Sprintf(" AND base.score < $%d", len(params))
	}

	if len(query.Languages) > 0 {
		params = append(params, query.Languages)
		queryString += fmt.Sprintf(" AND ALL language IN base.languages SATISFIES language IN $%d END", len(params))
	}

	if query.ScrapingStatus != nil {
		params = append(params, query.ScrapingStatus.String())
		queryString += fmt.Sprintf(" AND base.scraping_status = $%d", len(params))
	}

	return queryString, params
}
