package couchbase

import (
	"strconv"
	"strings"
)

// IDSeparator is the separator used to separate document id parts
const IDSeparator = ":"

// CounterDocumentValue is used to unmarshal counter documents.
type CounterDocumentValue int

// LookupKey is used to represent lookup keys inside a database.
type LookupKey struct {
	Key string `json:"key"`
}

// CountResult is used to unmarshal Count() n1ql queries.
type CountResult struct {
	Count int `json:"count"`
}

// IDResult is used to unmarshal id n1ql queries.
type IDResult struct {
	ID string `json:"id"`
}

// SerializeKey serializes the given key parts into a key string.
func SerializeKey(docType DocumentType, parts ...string) string {
	return strings.Join(append([]string{string(docType)}, parts...), IDSeparator)
}

// GetKeyType returns the keys document type
func GetKeyType(key string) DocumentType {
	return DocumentType(strings.SplitN(key, IDSeparator, 2)[0])
}

// UnmarshalJSON is the custom CounterDocumentValue json unmarshaler.
func (v *CounterDocumentValue) UnmarshalJSON(data []byte) error {
	value, err := strconv.Atoi(string(data))
	if err != nil {
		return err
	}

	*v = CounterDocumentValue(value)
	return nil
}

// Int returns the in the CounterDocumentValue contained int.
func (v CounterDocumentValue) Int() int {
	return int(v)
}
