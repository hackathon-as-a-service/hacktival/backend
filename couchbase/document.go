package couchbase

import (
	"strconv"
)

const (
	// LeadType is the document type of lead documents.
	LeadType DocumentType = "lead"

	// ContributionCalendarEntry is the document type of contribution calendar entries.
	ContributionCalendarEntry = "cotribclandarentry"

	// RepositoryType is the type of repository documents.
	RepositoryType = "repo"
)

// DocumentType represents a couchbase document type
type DocumentType string

// MarshalJSON is the custom DocumentType json marshaller
func (t DocumentType) MarshalJSON() ([]byte, error) {
	return []byte(strconv.Quote(string(t))), nil
}

// NewDocument creates a new couchbase document.
func NewDocument(docType DocumentType, version int, base interface{}) *Document {
	return &Document{
		Version: version,
		Type:    docType,
		Base:    base,
	}
}

// Document represents a couchbase document.
type Document struct {
	Base    interface{}  `json:"base"`
	Type    DocumentType `json:"type"`
	Version int          `json:"version"`
}
