package couchbase

import (
	"github.com/couchbase/gocb"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/repository"
)

type repoRepository struct {
	bucket *gocb.Bucket
}

// NewRepoRepository creates a new couchbase based repo.Repository
func NewRepoRepository(bucket *gocb.Bucket) repository.Repository {
	return &repoRepository{
		bucket: bucket,
	}
}

func (r *repoRepository) GetByLeadID(id string) ([]*repository.Repo, error) {
	params := []interface{}{RepositoryType, id}
	queryString := "SELECT bacon.* FROM `%s` bacon WHERE type = $1 AND base.lead_id = $2"

	res, err := r.bucket.ExecuteN1qlQuery(gocb.NewN1qlQuery(queryString), params)
	if err != nil {
		return nil, err
	}

	repos := []*repository.Repo{}
	doc := &Document{Base: &repository.Repo{}}
	for res.Next(doc) {
		repos = append(repos, doc.Base.(*repository.Repo))
		doc = &Document{Base: &repository.Repo{}}
	}

	return repos, nil
}

// Get returns the lead with the given id.
func (r *repoRepository) Get(id string) (*repository.Repo, error) {
	doc := &Document{Base: &repository.Repo{}}
	if _, err := r.bucket.Get(SerializeKey(RepositoryType, id), doc); err != nil {
		return nil, err
	}

	return doc.Base.(*repository.Repo), nil
}

// Insert inserts the given lead into the database.
func (r *repoRepository) Insert(repo *repository.Repo) error {
	_, err := r.bucket.Insert(SerializeKey(RepositoryType, repo.ID), repo, 0)
	return err
}

// Update updates the given lead, the lead will be identified by his id.
func (r *repoRepository) Update(repo *repository.Repo) error {
	_, err := r.bucket.Replace(SerializeKey(RepositoryType, repo.ID), repo, 0, 0)
	return err
}

// UpsertMany inserts the given entries into the database or updates them if they
// already exist
func (r *repoRepository) UpsertMany(repos []*repository.Repo) error {
	blkOP := make([]gocb.BulkOp, len(repos))

	for i, r := range repos {
		blkOP[i] = &gocb.UpsertOp{
			Key:   SerializeKey(RepositoryType, r.ID),
			Value: NewDocument(RepositoryType, 1, r),
		}
	}

	if err := r.bucket.Do(blkOP); err != nil {
		return err
	}

	for _, val := range blkOP {
		if err := val.(*gocb.UpsertOp).Err; err != nil {
			return err
		}
	}

	return nil
}

// Delete deletes the lead with the given id.
func (r *repoRepository) Delete(id string) error {
	_, err := r.bucket.Remove(SerializeKey(RepositoryType, id), 0)
	return err
}
