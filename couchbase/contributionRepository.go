package couchbase

import (
	"fmt"

	"github.com/couchbase/gocb"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/contribution"
)

type contributionRepository struct {
	bucket *gocb.Bucket
}

// NewContributionRespsitory creates a new couchbase based contribution.Repository
func NewContributionRespsitory(bucket *gocb.Bucket) contribution.Repository {
	return &contributionRepository{
		bucket: bucket,
	}
}

func (r *contributionRepository) Find(query *contribution.Query) (*contribution.Window, error) {
	params := []interface{}{ContributionCalendarEntry}
	queryString := fmt.Sprintf("SELECT bacon.* FROM `%s` bacon WHERE bacon.type = $1", r.bucket.Name())

	if query.LeadID != "" {
		params = append(params, query.LeadID)
		queryString += fmt.Sprintf(" AND base.lead_id = $%d", len(params))
	}

	if !query.From.IsZero() {
		params = append(params, query.From)
		queryString += fmt.Sprintf(" AND base.creation > $%d", len(params))
	}

	if !query.To.IsZero() {
		params = append(params, query.To)
		queryString += fmt.Sprintf(" AND base.creation < $%d", len(params))
	}

	res, err := r.bucket.ExecuteN1qlQuery(gocb.NewN1qlQuery(queryString), params)
	if err != nil {
		return nil, err
	}

	entries := []*contribution.CalendarEntry{}
	doc := &Document{Base: &contribution.CalendarEntry{}}
	for res.Next(doc) {
		entries = append(entries, doc.Base.(*contribution.CalendarEntry))
		doc = &Document{Base: &contribution.CalendarEntry{}}
	}

	return contribution.NewWindow(query.LeadID, query.From, query.To, entries), nil
}

func (r *contributionRepository) UpsertMany(contributions []*contribution.CalendarEntry) error {
	blkOP := make([]gocb.BulkOp, len(contributions))

	for i, c := range contributions {
		blkOP[i] = &gocb.UpsertOp{
			Key:   SerializeKey(ContributionCalendarEntry, c.ID),
			Value: NewDocument(ContributionCalendarEntry, 1, c),
		}
	}

	if err := r.bucket.Do(blkOP); err != nil {
		return err
	}

	for _, val := range blkOP {
		if err := val.(*gocb.UpsertOp).Err; err != nil {
			return err
		}
	}

	return nil
}
