package graph

import (
	"context"

	"gitlab.com/hackathon-as-a-service/hacktival/backend/graphql/model"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/lead"
)

// queryResolver is the graphQL query resolver.
type queryResolver struct {
	leadSrv lead.Service
}

// NewQueryResolver creates a new graphQL query resolver.
func NewQueryResolver(leadSrv lead.Service) QueryResolver {
	return &queryResolver{
		leadSrv: leadSrv,
	}
}

// GetLead is the graphQL query resolver for query.GetLead.
func (r *queryResolver) GetLead(ctx context.Context, id string) (*lead.Lead, error) {
	return r.leadSrv.Get(id)
}

// FindLeads is the graphQL query resolver for query.FindLeads.
func (r *queryResolver) FindLeads(ctx context.Context, q model.LeadQuery) (*lead.Window, error) {
	sortOrder, err := lead.SortOrderFromString(q.OrderType)
	if err != nil {
		return nil, err
	}

	minscore := 0.0
	if q.MinScore != nil {
		minscore = *q.MinScore
	}

	maxscore := 0.0
	if q.MaxScore != nil {
		maxscore = *q.MaxScore
	}

	cursor := ""
	if q.Cursor != nil {
		cursor = *q.Cursor
	}

	scrapingStatus := lead.Scraped

	return r.leadSrv.Find(&lead.Query{
		ScrapingStatus: &scrapingStatus,
		Cursor:         cursor,
		Languages:      q.Languages,
		MinScore:       minscore,
		MaxScore:       maxscore,
		Next:           q.Next,
		OrderType:      sortOrder,
		Ascending:      q.Ascending,
	})
}

// GetNextToScrape returns the next profiles for the scraper to scrape.
func (r *queryResolver) GetNextToScrape(ctx context.Context, count int) ([]lead.Lead, error) {
	leads, err := r.leadSrv.GetNextToScrape(count)
	if err != nil {
		return nil, err
	}

	result := make([]lead.Lead, len(leads))
	for i, l := range leads {
		result[i] = *l
	}

	return result, nil
}
