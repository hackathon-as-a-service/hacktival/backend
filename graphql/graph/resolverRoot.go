package graph

type resolverRoot struct {
	mutationResolver   MutationResolver
	queryResolver      QueryResolver
	leadResolver       LeadResolver
	leadWindowResolver LeadWindowResolver
}

func NewResolverRoot(mutationResolver MutationResolver, queryResolver QueryResolver,
	leadResolver LeadResolver, leadWindowResolver LeadWindowResolver) ResolverRoot {
	return &resolverRoot{
		mutationResolver:   mutationResolver,
		leadResolver:       leadResolver,
		queryResolver:      queryResolver,
		leadWindowResolver: leadWindowResolver,
	}
}

func (r *resolverRoot) Mutation() MutationResolver {
	return r.mutationResolver
}

func (r *resolverRoot) Query() QueryResolver {
	return r.queryResolver
}

func (r *resolverRoot) Lead() LeadResolver {
	return r.leadResolver
}

func (r *resolverRoot) LeadWindow() LeadWindowResolver {
	return r.leadWindowResolver
}
