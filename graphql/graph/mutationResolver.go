package graph

import (
	"context"

	"gitlab.com/hackathon-as-a-service/hacktival/backend/contribution"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/lead"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/repository"
)

// mutationResolver is the graphQL mutation resolver.
type mutationResolver struct {
	leadSrv         lead.Service
	repositorySrv   repository.Service
	contributionSrv contribution.Service
}

// NewMutationResolver creates a new graphQL mutation resolver.
func NewMutationResolver(leadSrv lead.Service, repositorySrv repository.Service, contributionSrv contribution.Service) MutationResolver {
	return &mutationResolver{
		leadSrv:         leadSrv,
		repositorySrv:   repositorySrv,
		contributionSrv: contributionSrv,
	}
}

func (r *mutationResolver) CalculateLeadScore(ctx context.Context, id string) (bool, error) {
	if _, err := r.leadSrv.Evaluate(id); err != nil {
		return false, err
	}

	return true, nil
}

func (r *mutationResolver) UpsertRepositories(ctx context.Context, repositories []repository.Params) (bool, error) {
	params := make([]*repository.Params, len(repositories))
	for i, r := range repositories {
		params[i] = &r
	}

	if err := r.repositorySrv.UpsertMany(params); err != nil {
		return false, err
	}

	return true, nil
}

func (r *mutationResolver) UpsertLeads(ctx context.Context, leads []lead.Params) (bool, error) {
	params := make([]*lead.Params, len(leads))
	for i, l := range leads {
		params[i] = &l
	}

	if err := r.leadSrv.UpsertMany(params); err != nil {
		return false, err
	}

	return true, nil
}

func (r *mutationResolver) UpsertContributions(ctx context.Context, contributions []contribution.CalendarEntryParams) (bool, error) {
	params := make([]*contribution.CalendarEntryParams, len(contributions))
	for i, c := range contributions {
		params[i] = &c
	}

	if err := r.contributionSrv.UpsertMany(params); err != nil {
		return false, err
	}

	return true, nil
}
