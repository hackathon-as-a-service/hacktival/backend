package graph

import (
	"context"
	"time"

	"gitlab.com/hackathon-as-a-service/hacktival/backend/contribution"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/graphql/model"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/lead"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/repository"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/uuid"
)

type leadResolver struct {
	repoSrv         repository.Service
	contributionSrv contribution.Service
}

func NewLeadResolver(repoSrv repository.Service, contributionSrv contribution.Service) LeadResolver {
	return &leadResolver{
		repoSrv:         repoSrv,
		contributionSrv: contributionSrv,
	}
}

func (r *leadResolver) ScrapingStatus(ctx context.Context, obj *lead.Lead) (string, error) {
	return obj.ScrapingStatus.String(), nil
}

func (r *leadResolver) LanguageScores(ctx context.Context, obj *lead.Lead) ([]model.LanguageScore, error) {
	result := []model.LanguageScore{}
	for lang, score := range obj.LanguageScores {
		result = append(result, model.LanguageScore{
			ID:       uuid.New(),
			Score:    score.Score,
			Language: lang,
		})
	}

	return result, nil
}

func (r *leadResolver) Repositories(ctx context.Context, obj *lead.Lead) ([]repository.Repo, error) {
	repos, err := r.repoSrv.GetByLeadID(obj.ID)
	if err != nil {
		return nil, err
	}

	result := make([]repository.Repo, len(repos))
	for i, r := range repos {
		result[i] = *r
	}

	return result, nil
}

func (r *leadResolver) Contributions(ctx context.Context, obj *lead.Lead) ([]contribution.CalendarEntry, error) {
	window, err := r.contributionSrv.Find(&contribution.Query{
		LeadID: obj.ID,
		From:   time.Now().AddDate(-1, 0, 0),
	})

	if err != nil {
		return nil, err
	}

	result := make([]contribution.CalendarEntry, len(window.Edges))
	for i, e := range window.Edges {
		result[i] = *e
	}

	return result, nil
}
