package graph

import (
	"context"

	"gitlab.com/hackathon-as-a-service/hacktival/backend/lead"
)

type leadWindowResolver struct {
	leadSrv lead.Service
}

func NewLeadWindowResolver(leadSrv lead.Service) LeadWindowResolver {
	return &leadWindowResolver{
		leadSrv: leadSrv,
	}
}

func (r *leadWindowResolver) TotalCount(ctx context.Context, obj *lead.Window) (int, error) {
	return r.leadSrv.GetTotalCount(obj)
}
