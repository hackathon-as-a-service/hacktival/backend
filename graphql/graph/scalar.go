package graph

import (
	"encoding/json"
	"errors"
	"io"
	"strconv"
	"time"

	"github.com/99designs/gqlgen/graphql"
)

// MarshalDateTime is the date time scalar marshaller.
func MarshalDateTime(t time.Time) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		io.WriteString(w, strconv.FormatInt(t.Unix(), 10))
	})
}

// UnmarshalDateTime is the date time scalar un-marshaller.
func UnmarshalDateTime(v interface{}) (time.Time, error) {
	if s, ok := v.(json.Number); ok {
		i, err := s.Int64()
		if err != nil {
			return time.Time{}, err
		}

		return time.Unix(i, 0), nil
	}

	if i, ok := v.(int64); ok {
		return time.Unix(i, 0), nil
	}

	return time.Time{}, errors.New("date should be a unix timestamp")
}
