package permission

import "errors"

var (
	// ErrPermissionDenied occurs when a accessor performs an operation he isn't allowed to.
	ErrPermissionDenied = errors.New("permission denied")
)
