package permission

import "context"

// ContextKey is used to store package related data inside a context.
type ContextKey string

const (
	// AccessorContextKey is used to retrieve and store the current accessor inside a gql request context.
	AccessorContextKey ContextKey = "accessor"
)

// Accessor represents a single accessor
type Accessor struct {
	IsClient bool
}

// NewAccessor creates a new accessor
func NewAccessor(isClient bool) *Accessor {
	return &Accessor{
		IsClient: isClient,
	}
}

// InjectAccessor is used to inject a accessor into a request context.
func InjectAccessor(ctx context.Context, acr *Accessor) context.Context {
	return context.WithValue(ctx, AccessorContextKey, acr)
}

// AccessorFromContext returns the in the context contained accessor
func AccessorFromContext(ctx context.Context) *Accessor {
	acr, ok := ctx.Value(AccessorContextKey).(*Accessor)
	if !ok {
		return nil
	}

	return acr
}

// AssertClient returns an error if the in the context contained accessor isn't a client.
func AssertClient(ctx context.Context) error {
	acr := AccessorFromContext(ctx)
	if acr == nil || !acr.IsClient {
		return ErrPermissionDenied
	}

	return nil
}
