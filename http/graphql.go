package http

import (
	"net/http"
	"time"

	"github.com/99designs/gqlgen/handler"
	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/config"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/graphql/graph"
	"gitlab.com/hackathon-as-a-service/hacktival/backend/permission"
)

const (
	// GraphQLEndpointRoute is the http route used to handle graphql requests.
	GraphQLEndpointRoute = "/graphql"

	// GraphQLPlaygroundRoute is the http route used to serve the graphql playground.
	GraphQLPlaygroundRoute = "/playground"
)

// GraphQLHandler is the graphql http handler
type GraphQLHandler struct {
	handlerFunc          http.HandlerFunc
	playGroundHandleFunc http.HandlerFunc
}

// NewGraphQL creates a new http graphql handler.
func NewGraphQL(resolverRoot graph.ResolverRoot, c config.Config) *GraphQLHandler {
	return &GraphQLHandler{
		playGroundHandleFunc: handler.Playground("Bacon API Playground", GraphQLEndpointRoute),
		handlerFunc: handler.GraphQL(graph.NewExecutableSchema(graph.Config{Resolvers: resolverRoot}),
			handler.WebsocketUpgrader(websocket.Upgrader{
				CheckOrigin: wsCheckOriginFunc(c.CORSAllowedOrigins),
			}),
			handler.WebsocketKeepAliveDuration(time.Second*30),
		),
	}
}

// RegisterRoutes registers the handlers routes on the given router
func (h *GraphQLHandler) RegisterRoutes(r chi.Router) {
	r.Get(GraphQLEndpointRoute, h.GraphQL)
	r.Post(GraphQLEndpointRoute, h.GraphQL)
	r.Get(GraphQLPlaygroundRoute, h.Playground)
}

// GraphQL handles GraphQL endpoint requests.
func (h *GraphQLHandler) GraphQL(w http.ResponseWriter, r *http.Request) {
	auth := r.Header.Get("Authorization")
	ctx := r.Context()

	if auth == "scraper-secure-auth-123" {
		ctx = permission.InjectAccessor(r.Context(), permission.NewAccessor(true))
	}

	h.handlerFunc(w, r.WithContext(ctx))
}

// Playground handles GraphQL playground requests.
func (h *GraphQLHandler) Playground(w http.ResponseWriter, r *http.Request) {
	h.playGroundHandleFunc(w, r)
}

// wsCheckOriginFunc returns a function which returns true is the origin of the given request is inside the allowed origins slice.
func wsCheckOriginFunc(allowedOrigins []string) func(r *http.Request) bool {
	oMap := make(map[string]struct{})
	for _, o := range allowedOrigins {
		oMap[o] = struct{}{}
	}

	return func(r *http.Request) bool {
		origin := r.Header.Get("Origin")

		_, exists := oMap[origin]
		return origin != "" && exists
	}
}
